let todos = [];

/**
 * Insert todo to database
 *@todo {id, title, completed}
 * */
exports.insert = todo => {
    const tobeTodo = {...todo, completed: false};
    todos.push(tobeTodo);
    return tobeTodo;
};

/**
 * update todo by Id
 * @return {todo || false}
 * @param todo
 * */
exports.updateById = todo => {
    const todoIdx = todos.findIndex(value => value.id === todo.id);
    if (todoIdx !== -1) {
        todos[todoIdx] = {...todos[todoIdx], ...todo};
        return todos[todoIdx];
    }
    return false;
};

/**
 * Delete todo by Id
 * @param { number} id
 * @return {boolean}
 * */
exports.deleteById = id => {
    const todoIdx = todos.findIndex(todo => todo.id === id);
    if (todoIdx === -1) {
        return false;
    }
    todos.splice(todoIdx, 1);
    return true;
};

/**
 * find all todos
 * @params {boolean} completed
 * @return {*[]}
 */
exports.getAllTodo = param => {
    if (!param) {
        return todos;
    } else {
        return todos.filter(value => value.completed === param.completed)
    }


};

