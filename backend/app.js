require('dotenv').config();
const express = require('express');
const router = require('./routes/apis/index');
const listEndpoints = require('express-list-endpoints');
const bodyParser = require('body-parser');


const app = express();

const port = process.env.PORT || 3000;
console.log(process.env.PORT);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/api', router);

app.listen(port, ()=> {
    console.log(`todo-app backend listening on port ${port}`);
    console.log('Registered Routes: ');
    console.log(listEndpoints(app));
});