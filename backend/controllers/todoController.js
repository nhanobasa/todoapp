// ### Build APIs
// - Create Todo (Create single todo for each time) (/api/createTodo)
// - Update Todo & Mark this todo as completed (/api/updateTodo)
// - Get All Todo (/api/todos)
// - Get All completed todos (/api/todos with params: completed: boolean)
// - Get All activated todos (/api/todos with params: completed: false)
// - Delete Todo (/api/deleteTodo)
// - Clear Completed todos (/api/clearCompleted)
// id, title, completed
const todoModel = require('../models/todo');

module.exports.createTodo = (req, res) => {
    const todo = req.body;
    return res.json(todoModel.insert(todo));
};

module.exports.updateTodo = (req, res) => {
    const todo = req.body;
    return res.json(todoModel.updateById(todo));
};

module.exports.getTodoList = (req, res) => {
    const {completed} = req.body;
    const todos = completed === null ? todoModel.getAllTodo() : todoModel.getAllTodo({completed: !!completed});
    return res.json(todos);
};

module.exports.deleteTodo = (req, res) => {
    const {id} = req.body;
    const result = todoModel.deleteById(id);
    return res.json(result);
};

module.exports.clearCompleted = (_, res) => {
    const completed = todoModel.getAllTodo({completed: true});
    completed.forEach(
        todo => {
            todoModel.deleteById(todo.id);
            console.log(todoModel.getAllTodo({completed: true}));
            console.log(todo.id);
        }
    );
    console.log(todoModel.getAllTodo({completed: true}));
    res.json({result: true});
};